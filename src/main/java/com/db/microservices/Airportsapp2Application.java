package com.db.microservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Airportsapp2Application {
	public static void main(String[] args) {
		SpringApplication.run(Airportsapp2Application.class, args);
	}

}
